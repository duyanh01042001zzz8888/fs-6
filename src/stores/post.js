import {atom} from 'recoil';

export const list = atom({
    key: 'list',
    default: [],
});

export const listFilter = atom({
    key: 'listFilter',
    default: null
});
