import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Post from "./Post";
import {useEffect} from "react";
import {useRecoilState} from "recoil";
import {list as _postList} from "../../stores/post";

const PostLayout = () => {

    const [postList, setPostList] = useRecoilState(_postList);

    return (<Row lg={1} xl={2} className="g-4">
        {postList.map((ps) => (
            ps.map((p) => (
                <Post data={p}/>
            ))
        ))}
    </Row>)
}

export default PostLayout;