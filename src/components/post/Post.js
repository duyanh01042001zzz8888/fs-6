import {Link, useNavigate} from "react-router-dom";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import styles from "../../styles/post/Post.module.scss"
import classNames from "classnames";
import {useState} from "react";

const Post = (props) => {

    const navigate = useNavigate()

    const [hover, setHover] = useState(false)

    const thumbnailStyle = {
        background: "url(" + props.data.thumbnail + ") 50% 50% / calc(100% ) 100% no-repeat"
    }

    const openPost = () => {
        navigate("/post/"+props.data.id)
    }

    return (
        <Col>
            <Card onClick={openPost} className={classNames(styles.card, hover ? "shadow" : null)}
                  onMouseEnter={() => setHover(true)}
                  onMouseLeave={() => setHover(false)}>
                <div className={"d-flex"}>
                    <div className={classNames(styles.thumbnail)}>
                        <div style={thumbnailStyle} className={classNames(styles.img)}/>
                    </div>
                    <Card.Body>
                        <div>
                            <Card.Title>{props.data.title}</Card.Title>
                            <Card.Text>
                                {props.data.description}
                            </Card.Text>
                        </div>
                    </Card.Body>
                </div>
            </Card>
        </Col>
    )
}

export default Post;