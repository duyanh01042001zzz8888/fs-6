import {Link, useNavigate} from "react-router-dom";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import styles from "../../styles/post/Post.module.scss"
import classNames from "classnames";
import React, {Fragment, useState} from "react";
import dateFormat from "dateformat";
import timestamp from "unix-timestamp";
import {Button} from "react-bootstrap";

const PostComment = (props) => {
    const [limit, setLimit] = useState(10);

    return (
        <Fragment>
            {
                props.data.map((v, i) => {
                    // if (i+1 > limit) {
                    //     return null
                    // }

                    return (
                        <Fragment>
                            <div className="card mb-2">
                                <div className="card-body">
                                    <div className={"row"}>
                                        <div className={"col"}>
                                            <p><b>{v.name}</b></p>
                                        </div>
                                        <div className={"col"} style={{textAlign: "end"}}>
                                            <i className="fa-solid fa-thumbs-up"></i> {v.like}
                                            <span> - </span>
                                            {dateFormat(timestamp.toDate(v.date), 'HH "giờ" MM "phút," "ngày" dd "tháng" mm "năm" yyyy')}
                                        </div>
                                    </div>
                                    <div>
                                        <p>{v.content}</p>
                                    </div>

                                </div>
                            </div>
                            {
                                v.reply && v.reply.item &&
                                <Reply data={v.reply.item}/>
                            }
                        </Fragment>
                    )
                })
            }
            {/*<div className={classNames("pt-2", "d-flex", "justify-content-center")}>*/}
            {/*    <Button className={"w-50 pt-3 pb-3"}>*/}
            {/*        Show more*/}
            {/*    </Button>*/}
            {/*</div>*/}
        </Fragment>
    )
}

const Reply = (props) => {
    return (
        props.data.map((v, i) => {
            return (
                <div className="card m-5 mb-2 mt-0 me-0">
                    <div className="card-body">
                        <div className={"row"}>
                            <div className={"col"}>
                                <p><b>{v.name}</b></p>
                            </div>
                            <div className={"col"} style={{textAlign: "end"}}>
                                <i className="fa-solid fa-thumbs-up"></i> {v.like}
                                <span> - </span>
                                {dateFormat(timestamp.toDate(v.date), 'HH "giờ" MM "phút," "ngày" dd "tháng" mm "năm" yyyy')}
                            </div>
                        </div>
                        <div>
                            <p>{v.content}</p>
                        </div>
                    </div>
                </div>
            )
        })
    )
}
export default PostComment;