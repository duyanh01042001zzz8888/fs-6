import React, {Fragment, useEffect, useState, useRef} from 'react';
import classNames from "classnames";
import styles from "../../styles/post/Viewer.module.scss"
import timestamp from "unix-timestamp";

const PostViewer = (props) => {
    const [edit, setEdit] = useState();

    return (
        <div className={'border rounded p-2'}>
            {
                props.detail[0].map((v, i) => {
                    return <Line editorMode={props.editorMode} index={i} value={v} detail={props.detail} edit={[edit, setEdit]}/>
                })
            }
        </div>
    )
}

const Line = (props) => {
    const [hover, setHover] = useState(false);
    const [newValue, setNewValue] = useState(props.value.value);

    const [detail, setDetail] = useState(props.detail[0])

    const edit = () => {
        setNewValue(props.value.value);
        props.edit[1](props.index);
    }

    const del = () => {
        props.detail[1](props.detail[0].filter((o, i) => props.index !== i))
        setHover(false);
    }

    const save = () => {
        let temp = [...props.detail[0]]
        temp[props.index] = {...temp[props.index], value: newValue}
        props.detail[1](temp);
    }

    const onMouseEnter = () => {
        if (!props.editorMode) {
            return;
        }
        setHover(true);
    }
    const onMouseLeave = () => {
        if (props.edit[0] == props.index) {
            save();
            props.edit[1]();
        }
        setHover(false);
    }

    return (
        <div
            className={classNames("p-2", hover ? "shadow rounded bg-light" : null)}
            onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>

            {hover && (
                <div className="pb-1">
                    {
                        props.value.type == "text" &&
                        <button type="button" className="btn btn-primary m-1" onClick={edit}><i
                            className="fa-solid fa-pen"></i></button>
                    }
                    <button type="button" className="btn btn-danger m-1" onClick={del}><i
                        className="fa-solid fa-trash-can"></i></button>
                </div>
            )}

            {props.value.type == "text" && (
                <div>
                    {props.edit[0] == props.index ? (
                        <textarea className={'rounded w-100 mb-0 pb-0'} rows="4" cols="50" value={newValue}
                                  onChange={(e) => {
                                      setNewValue(e.target.value);
                                  }}/>
                    ) : (
                        <p className={classNames(styles.text)}>{props.value.value}</p>
                    )}
                </div>
            )}

            {props.value.type == "image" && (
                <div className={'text-center'}>
                    <img className={classNames(styles.image)} src={props.value.value}/>
                    <p className={classNames("pt-1", styles.text)}>{props.value.description}</p>
                </div>
            )}
        </div>
    )
}

export default PostViewer;
