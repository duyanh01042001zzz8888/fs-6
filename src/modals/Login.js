import React, {useState} from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import {useRecoilState} from "recoil";
import {auth as _auth} from "../stores/auth";
import post from "../libs/post";
import auth from "../libs/auth"
import {Spinner} from "react-bootstrap";

const Login = (props) => {

    const handleClose = () => props.setShow(false);
    const handleShow = () => props.setShow(true);

    const [useAuth, setAuth] = useRecoilState(_auth);

    const [useField, _setField] = useState({
        u: null,
        p: null
    })

    const [loading, setLoading] = useState(false);

    const setField = (k, v) => {
        _setField({...useField, [k]: v})
    }

    const login = async () => {
        setLoading(true);
        try {
            await new Promise(resolve => setTimeout(resolve, 600));
            let response = await auth.login(useField.u, useField.p);
            localStorage.setItem("auth", response["access_token"])
            response = await auth.auth(response["access_token"]);
            setAuth(response);
            if (props.callback) {
                props.callback();
            }
            props.setShow(false);
        } catch (error) {
            alert("Can't login");
        }
        setLoading(false);
    }
    return (
        <>
            <Modal
                show={props.show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Login</Modal.Title>
                </Modal.Header>
                {loading ? (
                    <div className={'p-5 text-center'}>
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div>
                ) : (
                    <div>
                        <Modal.Body>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Username</Form.Label>
                                <Form.Control type="text" placeholder="Enter username" value={useField.u}
                                              onChange={(e) => setField("u", e.target.value)}/>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" value={useField.p}
                                              onChange={(e) => setField("p", e.target.value)}/>
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>
                            <Button variant="primary" onClick={login}>Login</Button>
                        </Modal.Footer>
                    </div>
                )}
            </Modal>
        </>
    );
}

export default Login;