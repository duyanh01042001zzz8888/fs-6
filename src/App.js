import './App.css';
import Container from 'react-bootstrap/Container';
import classNames from "classnames";
import styles from "./styles/App.module.scss"
import Home from "./pages/Home";
import Post from "./pages/Post";
import NotFound from "./pages/NotFound";
import {
    Routes,
    Route,
} from "react-router-dom";
import auth from "./libs/auth";
import {useRecoilState} from "recoil";
import {auth as _auth} from "./stores/auth";
import {useEffect, useState} from "react";

const App = () => {
    const [useAuth, setAuth] = useRecoilState(_auth);

    const [loading, setLoading] = useState(true);

    const verifyAccessToken = async () => {
        try {
            const access_token = localStorage.getItem("auth");
            if (access_token) {
                const response = await auth.auth(access_token);
                setAuth(response);
            }
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(async () => {
        await new Promise(resolve => setTimeout(resolve, 600))
        verifyAccessToken();
        setLoading(false);
    }, [])

    return (
        <Container className={classNames("mx-auto", "pt-4", "pb-4", styles.container)}>
            {loading ? (
                <p>Loading...</p>
            ) : (
                <Routes>
                    <Route path="/post/:id" element={<Post/>}/>
                    <Route exact path="/" element={<Home/>}/>
                    <Route path="*" element={<NotFound/>}/>
                </Routes>
            )}
        </Container>
    );
}

export default App;
