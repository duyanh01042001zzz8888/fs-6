import api from "./api";

const auth = class {

    static async auth(t) {
        const formData = new FormData();

        const url = '/auth';
        const response = await api.post(url, formData,{
            headers: {
                'Authorization': `Bearer ${t}`
            }
        });

        if ('error' in response) {
            throw new Error(response.error);
        }
        return response;
    }

    static async login(u,p) {
        const formData = new FormData();
        formData.append('user', u);
        formData.append('password', p);

        const url = '/auth/login';
        const response = await api.post(url, formData);

        if ('error' in response) {
            throw new Error(response.error);
        }
        return response.data;
    }

}

export default auth;