import api from "./api";

const post = class {

    static async list(page, limit, search) {
        let params = {};
        params['page'] = page;
        params['limit'] = limit;
        if (search) {
            params['search'] = search;
        }

        const url = '/posts';
        const response = await api.get(url, {params: params});

        if ('error' in response) {
            throw new Error(response.error);
        }
        return response.data;
    }

    static async get(id) {
        const url = '/post/' + id;
        const response = await api.get(url);

        if ('error' in response) {
            throw new Error(response.error);
        }
        return response.data;
    }

    static async update(id, data, t) {
        const url = '/post/' + id;
        const response = await api.put(url, data, {
            headers: {
                'Authorization': `Bearer ${t}`
            }
        });

        if ('error' in response) {
            throw new Error(response.error);
        }
        return response.data;
    }

    static async delete(id, t) {
        const url = '/post/' + id;
        const response = await api.delete(url, {
            headers: {
                'Authorization': `Bearer ${t}`
            }
        });

        if ('error' in response) {
            throw new Error(response.error);
        }
        return response.data;
    }

}

export default post;