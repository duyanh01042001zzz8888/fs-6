import App from "../App";
import PostLayout from "../components/post/Layout";
import classNames from "classnames";
import {Button, Nav, Spinner} from "react-bootstrap";
import Container from "react-bootstrap/Container";
import React, {Fragment, useEffect, useState} from 'react';
import {
    useNavigate,
    useParams
} from "react-router-dom";
import post from "../libs/post";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import PostViewer from "../components/post/Viewer"
import PostComment from "../components/post/Comment"
import timestamp from "unix-timestamp";
import dateFormat from 'dateformat'
import Login from "../modals/Login"
import {useRecoilState} from "recoil";
import {auth as _auth} from "../stores/auth";
import auth from "../libs/auth"

const Post = () => {

    let {id} = useParams();
    const navigate = useNavigate()

    const [postInfo, setPostInfo] = useState();
    const [postDetail, setPostDetail] = useState();

    const [loginModal, setLoginModal] = useState(false);
    const [editorMode, setEditorMode] = useState(false);

    const [useAuth, setAuth] = useRecoilState(_auth);

    const getPost = async () => {
        try {
            await new Promise(resolve => setTimeout(resolve, 600));
            let response = await post.get(id);
            setPostInfo(response);
            setPostDetail(response.detail);
        } catch (error) {
            alert("Post not found");
            navigate("/");
        }
    }

    const switchEditor = () => {
        if (!useAuth) {
            return setLoginModal(true);
        }
        setEditorMode(true)
    }

    const editorDelete = async () => {
        try {
            await post.delete(postInfo.id, localStorage.getItem("auth"));
            navigate("/");
        } catch (error) {
            console.log(error);
        }
    }

    const editorDiscard = () => {
        setPostDetail(postInfo.detail);
        setEditorMode(false);
    }

    const editorPublish = async () => {
        try {
            console.log(postDetail);
            await post.update(postInfo.id, postDetail, localStorage.getItem("auth"));
        } catch (error) {
            console.log(error)
        }
        setEditorMode(false);
    }

    const loginSuccess = () => {
        setEditorMode(true)
    }

    useEffect(() => {
        getPost();
    }, [])

    return (
        <Fragment>
            {postInfo ? (
                <Fragment>
                    {
                        loginModal &&
                        <Login show={loginModal} setShow={setLoginModal} callback={loginSuccess}/>
                    }
                    <Nav className="navbar fixed-top navbar-light bg-light border-bottom">
                        <div className="container-fluid mt-1 mb-1" style={{maxWidth: '1320px'}}>
                            {editorMode ? (
                                <div className={'mr-auto w-100'}>
                                    <div className={'float-right'}>
                                        <Button className={'me-2'} variant="danger" onClick={editorDelete}><i
                                            className="fa-solid fa-trash me-1"></i> Delete</Button>
                                        <Button className={'me-2'} variant="secondary" onClick={editorDiscard}><i
                                            className="fa-solid fa-xmark me-1"></i> Discard</Button>
                                        <Button className={'me-2'} variant="primary" onClick={editorPublish}><i
                                            className="fa-solid fa-cloud-arrow-up me-1"></i> Publish</Button>
                                    </div>
                                </div>
                            ) : (
                                <Button variant="primary" onClick={switchEditor}><i
                                    className="fa-solid fa-pen me-1"></i> Switch editor mode</Button>
                            )}
                        </div>
                    </Nav>
                    <div className={classNames("pt-2 mt-5")}>
                        <h2>{postInfo.title}</h2>
                        <p className={'pt-1'}><h6>{postInfo.description}</h6></p>
                        <p className={'pb-1'}>{dateFormat(timestamp.toDate(postInfo.date), 'HH "giờ" MM "phút," "ngày" dd "tháng" mm "năm" yyyy')}</p>
                        <PostViewer editorMode={editorMode} detail={[postDetail, setPostDetail]}/>
                    </div>
                    <div className={classNames("pt-3 mt-3")}>
                        <h3 className={'pb-1'}>Comments</h3>
                        {postInfo.comment ?
                            <PostComment data={postInfo.comment}/> : (
                                <p>No comment</p>
                            )
                        }
                    </div>
                </Fragment>
            ) : (
                <div className={classNames("pt-5 pb-5", "w-100", "text-center")}>
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </div>
            )}
        </Fragment>
    )
}

export default Post;
