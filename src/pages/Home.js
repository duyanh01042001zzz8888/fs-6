import App from "../App";
import React, {Fragment, useEffect, useState} from 'react';
import PostLayout from "../components/post/Layout";
import classNames from "classnames";
import {Button, Nav, Spinner} from "react-bootstrap";
import Form from 'react-bootstrap/Form';
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import {useRecoilState} from "recoil";
import {list as _postList, listFilter as _listFilter} from "../stores/post";
import post from "../libs/post";
import InputGroup from 'react-bootstrap/InputGroup';
import {auth as _auth} from "../stores/auth";
import auth from "../libs/auth"

const Home = () => {
    const [postList, setPostList] = useRecoilState(_postList);
    const [listFilter, setListFilter] = useRecoilState(_listFilter);
    const [useAuth, setAuth] = useRecoilState(_auth);

    const [tempListFilter, setTempListFilter] = useState();
    const [showMoreState, setShowMoreState] = useState(false);

    const getPostList = async (page = 1) => {
        try {
            await new Promise(resolve => setTimeout(resolve, 600))
            let response = await post.list(page, 10, listFilter);
            setPostList([...postList, response]);
        } catch (error) {
            console.log(error);
        }
        setShowMoreState(false);
    }

    const showMore = () => {
        setShowMoreState(true);
        getPostList(postList.length + 1);
    }

    useEffect(() => {
        const init = async () => {
            if (!postList.length) {
                getPostList()
            }
            if (tempListFilter == null) {
                setTempListFilter(listFilter)
            }
        }
        init()
    }, [])

    useEffect(() => {
        if (tempListFilter == listFilter) {
            return;
        }
        const timeOutId = setTimeout(() => {
            setPostList([]);
            setListFilter(tempListFilter)
        }, 500);
        return () => clearTimeout(timeOutId);
    }, [tempListFilter]);

    useEffect(() => {
        if (!listFilter && postList.length) {
            return
        }
        getPostList(1)
    }, [listFilter]);

    return (
        <Fragment>
            <Nav className="navbar fixed-top navbar-light bg-light border-bottom">
                <div className="container-fluid mt-1 mb-1" style={{maxWidth: '1320px'}}>
                    <Row xs={2} md={3} xl={1} className={'mw-80'}>
                        <Form.Label className={'w-100'}>Search articles in title, description and details</Form.Label>
                        <InputGroup>
                            <InputGroup.Text id="basic-addon1"><i
                                className="fa-solid fa-magnifying-glass"></i></InputGroup.Text>
                            <Form.Control placeholder="Search something"
                                          disabled={!postList.length ? true : false} onChange={(e) => {
                                setTempListFilter(e.target.value)
                            }} value={tempListFilter} name={'searchInput'}/>
                        </InputGroup>
                    </Row>
                </div>
            </Nav>

            <div className={classNames("pt-5 mt-5")}>
                {!postList.length ? (
                    <div className={classNames("pt-4 pb-5", "w-100", "text-center")}>
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div>
                ) : (
                    <Fragment>
                        <PostLayout/>
                        <div className={classNames("pt-4", "d-flex", "justify-content-center")}>
                            <Button className={"w-50 pt-3 pb-3"} onClick={showMore} disabled={showMoreState}>
                                Show more
                            </Button>
                        </div>
                    </Fragment>
                )}
            </div>
        </Fragment>
    )
}

export default Home;
