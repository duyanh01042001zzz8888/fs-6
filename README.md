# VNExpress Crawler

### [Watch preview on Youtube](https://youtu.be/hxkg9uxrTjc)

## API
```
git clone https://gitlab.com/duyanh01042001zzz8888/fs-2.git
docker-compose -p vnec up --build
```
Start crawler to get sample data
```
docker exec -it vnec_app_1 sh -c "go run crawl.go"
```

## Frontend
```
docker build -t vnec_frontend .
```
```
docker run -it -p 3001:3001 vnec_frontend
```